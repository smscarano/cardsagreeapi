<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cards', function (Blueprint $table) {
            $table->id();
            $table->boolean('first_edition');
            $table->string('serial_number',200)->unique();
            $table->string('monster_type',100);
            $table->string('monster_subtype',100);
            $table->integer('atk')->unsigned();
            $table->integer('def')->unsigned();
            $table->integer('stars')->unsigned()->nullable();
            $table->string('description',1500);
            $table->decimal('price', $precision = 10, $scale = 3);
            $table->string('image',500);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cards');
    }
}
