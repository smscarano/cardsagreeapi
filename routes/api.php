<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//unauth routes
Route::post('register', 'App\Http\Controllers\UserController@register');
Route::post('login', 'App\Http\Controllers\UserController@authenticate');

//auth routes
Route::group(['middleware' => ['jwt.verify']], function() {
    //user routes
    Route::get('user','App\Http\Controllers\UserController@getAuthenticatedUser');
    Route::put('user/{id}','App\Http\Controllers\UserController@editUser');
    Route::delete('user/{id}','App\Http\Controllers\UserController@deleteUser');
    Route::get('users','App\Http\Controllers\UserController@listUsers');

    //cards routes
    Route::post('card','App\Http\Controllers\CardController@create');
    Route::get('card/{id}','App\Http\Controllers\CardController@view');
    Route::put('card/{id}','App\Http\Controllers\CardController@edit');
    Route::delete('card/{id}','App\Http\Controllers\CardController@delete');
    Route::get('cards','App\Http\Controllers\CardController@index');
});