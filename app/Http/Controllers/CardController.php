<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Card;
use Illuminate\Support\Facades\DB;

class CardController extends Controller
{
    /**
    * Devuelve el objeto de la carta buscada
    */
    public function view(Request $request, $id){
        try {
            $card = Card::find($id);
            if (!$card ) {
                return response()->json(['card_not_found'], 404);
            }
            return response()->json(compact('card'));
        
             } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
                            return response()->json(['token_expired'], $e->getStatusCode());
             } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
                            return response()->json(['token_invalid'], $e->getStatusCode());
             } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
                            return response()->json(['token_absent'], $e->getStatusCode());
        }
    }

    /**
    * Crea una carta y retorna el objeto creado
    */
    public function create(Request $request)
    {
            $validator = Validator::make($request->all(), [
            'first_edition' => 'required|string|max:1',
            'serial_number' => 'required|string',
            'monster_type' => 'required|string',
            'monster_subtype' => 'required|string',
            'atk' => 'required',
            'def' => 'required',
            'description' => 'required|string',
            'price' => 'required',
            'image' => 'required|string',

        ]);
    

        if($validator->fails()){
                return response()->json($validator->errors()->toJson(), 400);
        }

        $card = Card::create([
            'first_edition' => $request->get('first_edition'),
            'serial_number' => $request->get('serial_number'),
            'monster_type' => $request->get('monster_type'),
            'monster_subtype' => $request->get('monster_subtype'),
            'atk' => $request->get('atk'),
            'def' => $request->get('def'),
            'stars' => $request->get('stars'),
            'description' => $request->get('description'),
            'price' => $request->get('price'),
            'image' => $request->get('image')
        ]);

        return response()->json(array('status'=>'ok','data'=>$card));
    }

    /**
    * Retorna un array con todas las cartas si no se pasan parámetros o
    * un subconjunto si se especifican parámetros query de búsqueda
    */

    public function index(Request $request){
        try {
            $searchable_fields = [
                'card_id' , 'first_edition' , 'serial_number' , 
                'monster_type' , 'monster_subtype' , 'atk' , 
                'defense' , 'stars' , 'description' , 
                'price' , 'image'
            ];
            $cards = DB::table('cards');
            foreach ($searchable_fields as $key => $search_field) {
                if(  $request->has($search_field)){
                        $cards = $cards->where($search_field, $request->get($search_field));
                }
            }
            if(  $request->has('order_field') && $request->has('order_direction') ){
                $cards = $cards->orderBy($request->get('order_field') , $request->get('order_direction') );
             }
            if(  $request->has('results_per_page')){
                $cards = $cards->paginate($request->get('results_per_page'));
             }
             else{
                $cards = $cards->paginate(5);
             }
            
            } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
                    return response()->json(['token_expired'], $e->getStatusCode());
            } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
                    return response()->json(['token_invalid'], $e->getStatusCode());
            } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
                    return response()->json(['token_absent'], $e->getStatusCode());
            }
            return response()->json(compact('cards'));
    }

    /**
    * Borra la carta seleccionada
    */
    public function delete(Request $request, $id){
        try {
            $card = Card::find($id);

            if (!$card ) {
                    return response()->json(['card_not_found'], 404);
            }
            $card->delete();
            return response()->json(["status"=>"deleted"]);

            } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
                    return response()->json(['token_expired'], $e->getStatusCode());
            } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
                    return response()->json(['token_invalid'], $e->getStatusCode());
            } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
                    return response()->json(['token_absent'], $e->getStatusCode());
            }
    }

    /**
    * Edita la carta seleccionada
    */
    public function edit(Request $request,$id){
        try {
            $card = Card::find($id);
            if (!$card ) {
                    return response()->json(['card_not_found'], 404);
            }
            $edited = false;
            $editable_fields = [
                'card_id' , 'first_edition' , 'serial_number' , 
                'monster_type' , 'monster_subtype' , 'atk' , 
                'defense' , 'stars' , 'description' , 
                'price' , 'image'
            ];
            
            foreach ($editable_fields as $key => $editable_field) {
                if(  $request->get($editable_field) && $request->get($editable_field) != $card->$editable_field ){
                        $card->$editable_field = $request->$editable_field;
                        $edited = true;
                }
            }
            if($edited == true){
                $card->save();
                return response()->json($card);
            }
            return response()->json(['status' =>'Nothing to edit']);
    
            } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
                    return response()->json(['token_expired'], $e->getStatusCode());
            } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
                    return response()->json(['token_invalid'], $e->getStatusCode());
            } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
                    return response()->json(['token_absent'], $e->getStatusCode());
        }
    }
}
