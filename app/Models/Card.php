<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    use HasFactory;

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_edition',
        'serial_number',
        'monster_type',
        'monster_subtype',
        'atk',
        'def',
        'stars',
        'description',
        'price',
        'image'
    ];
}
