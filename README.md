# Cartas JSON Restful API

## Descripción
Restful JSON API de cartas de Yu-Gi-Oh! utilizando Laravel.

## Suposiciones realizadas
Utilizar JSON como formato de datos.
Todas las búsquedas del filtro son claúsulas WHERE =  
Los campos atk, def y stars son enteros sin firmar.
El campo first_edition se asume como booleano y se implementa con tinyint, boolean en el caso de la migración de Laravel.  
Los campos serial_number, monster_type y monster subtype se asumen como strings.
El campo serial_number se asume como unique.
El campo precio se asume como decimal.
El campo image se asume como string, evaluando que se insertaría en el el path al recurso.  

## Ejecución del proyecto
Se construyo la API utilizando el lenguaje PHP y el framework Laravel. El patrón utilizado fue MVC.  
Para las consultas a la base de datos se utilizó el ORM de Laravel, Eloquent.  
Para encriptar las contraseñas de los usuarios se hizo uso de Bcrypt  
Se utilizó swagger para describir los endpoints  

## Requerimientos de software
 - php 7.*
 - php-intl
 - php-mbstrimg  
 - php-mysql
 - composer
 - Apache2 mod rewrite enabled
 - mysql server
 - git
 
## Instalación

`git clone https://smscarano@bitbucket.org/smscarano/cardsagreeapi.git`  
`cd cardsagreeapi`  
Instalar dependendencias (correr desde el root del directorio de la app)   
`composer install` o  `php composer.phar install`
  
## Setup
Copiar el archivo .env.example que se encuentra en el del proyecto a .env y editarlo para que refleje los valores deseados, aquí se configuran las credenciales para la base de datos.  
Crear jtw secret  
`php artisan jwt:secret` (correr desde el root del directorio de la app)  
Crear una base de datos mysql con el nombre cards  
`CREATE DATABASE cards` 


## DB Migrations
## CUIDADO ESTE COMANDO DROPEARA LAS TABLAS SI EXISTEN  
`php artisan migrate`  

## Autenticación  
La API usea JWT como autenticación y bcrypt para hashear los passwords. Tanto en el registro de usuario como en el login se devuelve un token jwt.

## Uso
Se debe agregar /api/ al final de la url del sistema. Por ejemplo http://127.0.0.1/cardsagreeapi/api/cards  
Todas las requests a la API deben contener el header `Content-Type` con el valor `application/json`   
Las request que se dirijan a endpoints que requieran autorización deben utilizar el header `Authorization` y como valor el string "Bearer token_value". Por ejemplo `Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjFcL2NhcmRzYWdyZWVhcGlcL3B1YmxpY1wvYXBpXC9yZWdpc3RlciIsImlhdCI6MTY0NzA3NzM4NywiZXhwIjoxNjQ3MDgwOTg3LCJuYmYiOjE2NDcwNzczODcsImp0aSI6InRoeDRBYUdKQm9XbW9UUk0iLCJzdWIiOjMsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.Ms3_fq1TCQvAxumQJlJhRmJpHDnP_7zNIFrl_xiSPAY`  
En el documento de sobre los endpoints se detalla su topología, parámetros, retornos, etc.  

### Crear un usuario  
El endpoint /register con el verbo POST permite crear un usuario.  
Datos de ejemplo  
`{
    "email" : "email@test.com",
    "password" : "password",
    "name" : "username"
}`

### Login
El endpoint /login con el verbo POST permite crear un usuario.  
Datos de ejemplo  
`{
    "email" : "email@test.com",
    "password" : "password",
    "name" : "username"
}`

## Listado de cartas
El endpoint cards/ mediante el verbo GET es el que devuelve todas las cartas o un subconjunto de ellas. Se pueden utilizar parámetros query como filtro con el nombre de cada campo. Otros parámetros adicionales son order_field que se refiere al campo a utilizar para ordenar los resultados de la búsqueda. Debe ir acompañado del parámetro order_direction cuyos valores pueden ser asc o desc.
El último parámetro es results_per_page que permite seleccionar cuantos resultados por página muestra la paginación.    
Ejemplo: http://127.0.0.1/cardsagreeapi/api/cards?order_field=id&order_direction=desc&monster_type=monster2  

## Ver carta
El enpoint card/{id} permite ver una carta, retorna un objeto JSON de la misma

## Crear carta
El enpoint newcard/ permite crear una carta utilizando el verbo POST. 
Datos de ejemplo:
`{
    "first_edition" : "0",
    "serial_number" : "s2sda",
    "monster_type" : "monster2",
    "monster_subtype" : "ice",
    "atk" : "21",
    "def" : "9",
    "description" : "ice monster",
    "price" : "11.2",
    "image" : "test@test.com"
}`

## Editar carta 
El enpoint card/{id} permite ver editar una carta utilizando el verbo PUT. Retorna un objeto JSON de la misma editada  
`{
    "monster_type" : "dragon",
    "monster_subtype" : "ice",
    "image" : "test@test.com"
}`

## Borrar carta 
El enpoint card/{id} permite ver borrar una carta utilizando el verbo DELETE.  





